# Vault integration

## Vault Configuration

Vault configuration:
* secret engine
* app role roles configuraiton
* Policy creation
* Secrets integration

[link](./vault-configuration/README.md)

## Kubernetes configuration

Per cluster configuration in order to consume the secrets stored in vault, per application: [link](./kubernetes-configuration/README.md)
