#!/bin/bash
#
# Setup

# MAIN
SCRIPT_DIR="$(dirname "$0")"
# shellcheck disable=SC1091
source "$SCRIPT_DIR/include.sh"

find "$DATA_DIR" -name 'policy*.hcl' -delete

# Global/Common/Shared secrets
createSharedSecretsPolicies

# Policies identical per environment
createEnvironmentsPolicies

# Create the per-environment/application policies
for environment in production ci; do
  createPerEnvironmentPolicies $environment
done

# Generic per developper policy using GitLab Auth metadata
#createPerEnvironmentPolicies '{{identity.entity.aliases.gitlab.id}}' developer

echo "Policies configuration file created in $DATA_DIR."
echo "Run run-policies.sh to create/update them."