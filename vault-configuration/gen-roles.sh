#!/usr/bin/env bash
# 
# Generate the vault role command for an environment / application
# 

createRoleCmd() {
    ENVIRONMENT=$1
    APPLICATION=$2
    POLICIES=$(awk -F, -v application="$APPLICATION" -v environment="$ENVIRONMENT" '(NR>1) && ($1==application) && ($2==environment) {print $3}' "$POLICIES_MAP" | tr '\n' ','|sed 's/,$//'
)
    if [[ -z $POLICIES ]];then
        echo "ERROR: application $APPLICATION and/or environment $ENVIRONMENT not found in policy map file ($POLICIES_MAP)" >&2
        exit 1
    fi
    printf "vault write auth/approle/role/%s-%s-role secret_id_ttl=10m token_num_uses=10 token_ttl=20m token_max_ttl=30m token_policies=default,%s\n" "$ENVIRONMENT" "$APPLICATION" "$POLICIES"
}

CURRENT_DIR="$(dirname "$0")"
ROLES_SCRIPT="$CURRENT_DIR/run-roles.sh"
POLICIES_MAP="$CURRENT_DIR/policy-map.csv"
echo '#!/usr/bin/env bash'>"$ROLES_SCRIPT"
awk -F, '(NR>1){print $1}' "$POLICIES_MAP"|sort -u|while read -r application;do
    awk -F, '(NR>1){print $2}' "$POLICIES_MAP"|sort -u|while read -r environment;do
        createRoleCmd "$environment" "$application" >> "$ROLES_SCRIPT"
    done
done
# List the roles
echo "vault list auth/approle/role" >> "$ROLES_SCRIPT"

chmod +x "$ROLES_SCRIPT"
echo "Creation role script generated: $ROLES_SCRIPT"