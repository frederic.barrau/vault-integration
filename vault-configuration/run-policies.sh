#!/usr/bin/env bash
# 
# Create / Update the policies created by gen-policies
SCRIPT_DIR="$(dirname "$0")"
DATA_DIR="$(realpath "$SCRIPT_DIR"/data)"

find "$DATA_DIR" -name 'policy*.hcl' -type f -maxdepth 1|tr '\n' '\0'| xargs -n1 -0 -I{} /bin/bash -c 'vault policy write "$(basename "$1" .hcl)" "$1"' - "{}"