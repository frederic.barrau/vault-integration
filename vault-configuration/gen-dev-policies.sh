#!/usr/bin/env bash
#
#

# MAIN
SCRIPT_DIR="$(dirname "$0")"
# shellcheck disable=SC1091
source "$SCRIPT_DIR/include.sh"

if [[ $# -ne 1 ]];then
    printf "
    Usage: %s <name of the cluster>
    Example:
    %s dev-env1
    " "$(basename "$0")" "$(basename "$0")"
    exit 1
fi

ENVIRONMENT=$1
createPerEnvironmentPolicies "$ENVIRONMENT"
