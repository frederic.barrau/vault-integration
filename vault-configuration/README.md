# Vault Configuration

> These script uses the **Vault CLI** in order to create the policies
> The vault CLI must be authenticated with a token that have the create policies privilege.

## Global configuration

### AppRole auth engine

> Already done: all good

### Secret engine

Enable a KV2 secret engine with versionning available at `/secret`

```console
$ vault secrets enable -version=2 -path secret kv
```

## Policies

The policy definition is managed by the script [gen-policy.sh](./gen-policies.sh); this script will generate the configuration files for creating the all the policies:
* global secrets policies
* policies for secrets that have only 2 environments : production/development
* specific policies for each environment/application

Run  [run-policies.sh](./policies.sh) script to actually create the policies into vault.

## AppRole roles

* Setup the AppRole configuration. (Already done)
* Fill the policy map file in order to setup the policy per environment / application : [policy-map.csv](./policy-map.csv)
* Generate the roles creation script from the policy map using the [gen-roles.sh](./gen-roles.sh) script.
* Run the `run-roles.sh` script to create / update the roles & their policies in vault.


## Secrets initialization

> **Warning** the format of each secret is free, but the deployment configuration must match the format to fetch the secret.

### Global secrets

```console
$ vault kv put secret/shared/bridge/bridge-api-auth token="XXXX"
$ vault kv put secret/shared/maxmind/maxmind-api-auth token="XXXX"
```

### Environment-specific secrets

```console
$ vault kv put secret/shared/lyra/environments/development/lyra-api-auth token="XXXX"
$ vault kv put secret/shared/lyra/environments/production/lyra-api-auth token="XXXX"
$ vault kv put secret/shared/aws-s3-pleenk-data/environments/development/aws-s3-pleenk-data token="XXXX"
$ vault kv put secret/shared/aws-s3-pleenk-data/environments/production/aws-s3-pleenk-data token="XXXX"
```

### Per-environment secrets

Per environment/cluster secrets:

```bash
$ ENVIRONMENT=development
$ vault kv put secret/environments/$ENVIRONMENT/internals/bff/bff-jwt-encrypt-private private="xxxx"
$ vault kv put secret/environments/$ENVIRONMENT/internals/auth/auth-jwt-encrypt-private private="xxx"
$ vault kv put secret/environments/$ENVIRONMENT/internals/marketplace/marketplace-jwt-sign-private private="xxx"
$ vault kv put secret/environments/$ENVIRONMENT/internals/bff/shared/auth-jwt-sign-public public="xxx"
$ vault kv put secret/environments/$ENVIRONMENT/internals/auth/shared/bff-jwt-encrypt-public public="xxx"
$ vault kv put secret/environments/$ENVIRONMENT/internals/marketplace/shared/bff-jwt-encrypt-public public="xxx"
```
