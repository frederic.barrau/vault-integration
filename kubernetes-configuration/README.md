# Kubernetes configuration

## Pre-requisistes

On the "client" clusters:

* Vault Injector should be installed using Helm

See [here](https://www.vaultproject.io/docs/platform/k8s/helm/configuration#injector) for other option (replicas..)

```bash
RELEASE_VERSION=0.21.0 # Vault 1.11.2
VAULT_HOST=https://vault.svc
# Vault Root CA
VAULT_CA=./vault-ca.pem

# Create the helm configuration file
cat<<EOH > helm-vault-injector.yaml 
global:
  enabled: false
  externalVaultAddr: $VAULT_HOST
injector:
  enabled: true
  externalVaultAddr: VAULT_HOST
EOH

# ADD Vault Injector using the helm chart
helm repo add hashicorp https://helm.releases.hashicorp.com
helm repo update
helm upgrade vault hashicorp/vault \
  --namespace vault --version "$RELEASE_VERSION" \
  --values "helm-vault-injector.yaml
```

> Not possible to add the ROOT ca of the vault server ([related issue](https://github.com/hashicorp/vault-helm/issues/518))

* The ROOT cert of the TLS connection to the vault server must be made available to each cluster as a Secret/ConfigMap
* ⚠️ **This must be done for each namespace that will use this CA (each injector)**

```yaml
---
kind: Secret
apiVersion: v1
metadata:
  name: vault-root-ca-secret
stringData:
  ca-bundle.crt: |
    -----BEGIN CERTIFICATE-----
    ...
    -----END CERTIFICATE-----
type: Opaque
```

* Vault server should be reachable from the clusters's pods (FW)
* the Vault domain should be available in the clusters' DNS (either a global public DNS entry or a CoreDNS entry)

## AppRole Secrets into Kubernetes Secrets

For each application / cluster, a role in AppRole is created for accessing the proper secrets in Vault.
The AppRole access is made using a couple AppRole/RoleID secrets.
These secrets dedicated to a cluster should be added to the Kube secrets in order to setup the Vault Agent sidecar of each App.

For each Application / environment (= cluster name) replace the EnvVars ENVIRONMENT / PRODUCTION :

```bash
# Cluster's identificator  (production / ci / developer1 / ... )
ENVIRONMENT=production
# Kubectl's context dedicated to the previous environment
KCONTXT=production-cluster
# Application identification (bff/auth/marketplace)
APPLICATION=bff
# Getting the application role information
ROLE_ID=$(vault read -field=role_id auth/approle/role/$PRODUCTION-$APPLICATION-role/role-id|base64)
SECRET_ID=$(vault write -f role_id auth/approle/role/$PRODUCTION-$APPLICATION-role/secret-id|base64)

# Kube Template for creating the ConfigMap
TEMPLATE='apiVersion: v1
data:
  role-id: $ROLE_ID
  secret-id: $SECRET_ID
kind: Secret
metadata:
  name: vault-approle-$APPLICATION
  namespace: default
type: Opaque
'
# Create the secret this to the Kube's secrets
echo "$TEMPLATE"|envsubst|kubectl --context $KCONTXT apply -f -
```

> **WARNING** the secret IDs should be renewed regularily for each role (using a CI job for instance)

## Injector configuration

### Application injector configuration as ConfigMap

For each application, the name of the secrets and the envvars must be adjusted regarding the application's requirement:

Example for BFF/production:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: vault-sidecar-configmap-bff
data:
  config.hcl: |
    "auto_auth" = {
      "method" = {
        "type" = "approle"
        "config" = {
          "role_id_file_path" = "/vault/custom/role-id"
          "secret_id_file_path" = "/vault/custom/secret-id"
          "remove_secret_id_file_after_reading" = false
        }
      }

      "sink" = {
        "config" = {
          "path" = "/home/vault/.token"
        }

        "type" = "file"
      }
    }

    "exit_after_auth" = false
    "pid_file" = "/home/vault/.pid"

    "template" = {
      "contents" = <<EOT
        #
        # [ CUSTOMIZE THE SECRETS TO GET AND THE ENVARS VARS HERE: ]
        #
        
        # Global tokens
        {{with secret "secret/bridge/bridge-api-auth"}}
        export BRIDGE_API_TOKEN="{{ .Data.data.token }}"
        {{end}}

        {{with secret "secret/maxmind/maxmind-api-auth"}}
        export MAXMIND_API_TOKEN="{{ .Data.data.token }}"
        {{end}}

        # Per environment secrets

        {{with secret "secret/shared/lyra/environments/production/lyra-api-auth"}}
        export LYRA_API_TOKEN="{{ .Data.data.token }}"
        {{end}}

        {{with secret "secret/shared/lyra/environments/production/lyra-api-auth"}}
        export LYRA_API_TOKEN="{{ .Data.data.token }}"
        {{end}}

        {{with secret "secret/environments/production/internals/auth/shared/auth-jwt-encrypt-public"}}
        export AUTH_JWT_ENCRYPT_PUBLIC="{{ .Data.data.public }}"
        {{end}}

        {{with secret "environments/production/internals/bff/bff-jwt-encrypt-private"}}
        export BFF_JWT_ENCRYPT_PRIVATE="{{ .Data.data.private }}"
        {{end }}
    EOT
      "destination" = "/vault/secrets/envvars"
    }

  config-init.hcl: |
    "auto_auth" = {
      "method" = {
        "type" = "approle"
        "config" = {
          "role_id_file_path" = "/vault/custom/role-id"
          "secret_id_file_path" = "/vault/custom/secret-id"
          "remove_secret_id_file_after_reading" = false
        }
      }

      "sink" = {
        "config" = {
          "path" = "/home/vault/.token"
        }

        "type" = "file"
      }
    }

    "exit_after_auth" = true
    "pid_file" = "/home/vault/.pid"

```

### Deployment configuration patch

```yaml
spec:
  template:
    metadata:
      annotations:
        # Enable the injector
        vault.hashicorp.com/agent-inject: 'true'
        # Vault Root CA
        vault.hashicorp.com/ca-cert: /vault/tls/ca-bundle.crt
        vault.hashicorp.com/tls-secret: vault-root-ca-secret
        # [ CUSTOMIZE THE CONFIG MAP CONFIGURATION USING THE ONE DEDICATED TO THE CURRENT APP:]
        vault.hashicorp.com/agent-configmap: 'vault-sidecar-configmap-bff'

        # AppRole/SecretID used by the vault agent to authenticate [ CUSTOMIZE THE APPNAME / ENVIRONMENT ]
        vault.hashicorp.com/agent-extra-secret: vault-approle-bff
    spec:
      containers:
        - name: app
          # Customize the app startup by sourcing the envvars configuration file
          # generated by the vault agent:
          command: ['sh', '-c', 'source /vault/secrets/envvars && run_bff_app']
```